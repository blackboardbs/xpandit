@extends('flow.default')

@section('title') Clients @endsection

@section('header')
    <div class="container-fluid container-title float-left clear" style="margin-top: 1%;margin-bottom: 7px;">
        <h3 style="padding-bottom: 5px;">@yield('title')</h3>
        <div class="nav-btn-group" style="top:30%;padding-bottom: 5px;">
            <form autocomplete="off">
                <div class="form-row" style="flex-wrap: nowrap">
                    
                    <div class="form-group" style="display: inline-block">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                    <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')

        <div class="content-container page-content">
            <div class="row col-md-12 h-100 pr-0">
                <div class="container-fluid index-container-content h-100">
                    <!--<div class="table-responsive d-inline-block" style="margin-right:1.7%;height: 25%;width:49%;">
                        <table class="table table-borderless table-sm table-fixed">
                            <thead>
                            <tr>
                                <th nowrap style="box-shadow: none;"><h4>Birthdays</h4></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($clients_birthdays as $clientb)
                                <tr>
                                    <td>{{$clientb["company"]}} - {{$clientb["when"]}}</td>
                                </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no upcoming birthdays.</small></td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>-->
                    <div class="table-responsive d-inline-block pl-0" style="height: 25%;width:100%;">
                        <table class="table table-borderless table-sm table-fixed billboard-table">
                            <thead>
                            <tr>
                                <th nowrap style="box-shadow: none;"><h4>Billboard</h4></th>
                                <th class="last" nowrap style="box-shadow: none;vertical-align: top;">
                                    <a href="javascript:void(0)" onclick="composeBillboardMessage()" class="btn btn-sm btn-primary submit-btn" style="line-height: 1rem !important;">Add a message</a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($messages as $message)
                                <tr class="message-{{$message["id"]}}">
                                    <td class="billboard" colspan="100%"><a href="javascript:void(0)" style="display: block;" onclick="showBillboardMessage({{$message["id"]}})"></a>
                                        <span class="pull-right clickable close-icon" onclick="completeBillboardMessage({{$message["id"]}})" data-effect="fadeOut"><i class="fas fa-trash" style="color:#f06072"></i></span>
                                        <div class="card-block">
                                            <blockquote class="card-blockquote">
                                                <div class="blockquote-body" onclick="showBillboardMessage({{$message["id"]}})">@if($message["heading"] != null) <strong>{{$message["heading"]}}</strong><br />{{$message["message"]}} @else  {{$message["message"]}} @endif</div>
                                            </blockquote>
                                        </div>
                                    </td>
                                </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no messages to display.</small></td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>
                    {{--<div class="table-responsive d-inline-block pl-0" style="height: 25%;width:49%;">
                        <table class="table table-borderless table-sm table-fixed task-table">
                            <thead>
                            <tr>
                                <th nowrap style="box-shadow: none;"><h4>Tasks</h4></th>
                                <th class="last" nowrap style="box-shadow: none;vertical-align: top;">
                                    <a href="javascript:void(0)" onclick="composeUserTask()" class="btn btn-sm btn-primary submit-btn" style="line-height: 1rem !important;">Add a task</a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($user_tasks as $user_task)
                                <tr class="task-{{$user_task["id"]}}">
                                    <td class="usertask" colspan="100%">--}}{{--<a href="javascript:void(0)" style="display: block;" onclick="showBillboardMessage({{$message->id}})">{{$message->message}}</a>--}}{{--
                                        <span class="pull-right clickable close-icon" onclick="completeUserTask({{$user_task["id"]}})" data-effect="fadeOut"><input type="checkbox" /></span>
                                        <div class="card-block">
                                            <blockquote class="card-blockquote">
                                                <div class="blockquote-body" onclick="showUserTask({{$user_task["id"]}})"><strong>{{\Carbon\Carbon::parse($user_task["task_date_start"])->format('Y-m-d H:i')}} - {{\Carbon\Carbon::parse($user_task["task_date_start"])->format('Y-m-d H:i')}}</strong> : {{$user_task["task_type"]}}<br /><small class="text-muted">{{$user_task["client_name"]}}</small></div>
                                            </blockquote>
                                        </div>
                                    </td>
                                </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no tasks to display.</small></td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>--}}
                    @yield('header')
                    <div class="table-responsive w-100 float-left client-index" style="height: calc(66% - 30px);position:relative;">
                            <table class="table table-bordered table-hover table-sm table-fixed" style="max-height: calc(100% - 30px);">
                                <thead>
                                <tr>
                                    <th nowrap>@sortablelink('company', 'Name')</th>
                                    <th nowrap>@sortablelink('email', 'Email')</th>
                                    <th nowrap>@sortablelink('cell', 'Contact Number')</th>
                                    <th nowrap>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($clients as $client)
                                    <tr>
                                        <td><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["company"] ) && $client["company"] != ' ' ? $client["company"]  : 'Not Captured')}}</a></td>
                                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                                        <td>{{!is_null($client["contact"]) ? $client["contact"] : ''}}</td>
                                        <td class="last">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$client['id']}},{{$client['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$client['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$client['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-danger" onclick="submitForSignatures({{$client['id']}},{{$client["process_id"]}},{{$client["step_id"]}})" title="Submit an application for signatures"><i class="fas fa-share-square"></i> </a>
                                        </td>
                                    </tr >
                                @empty
                                    <tr>
                                        <td colspan="100%" class="text-center"><small class="text-muted">No patients match those criteria.</small></td>
                                    </tr>
                                @endforelse
                                </tbody>

                            </table>

                    </div>
                    <div style="position: relative;float:left;bottom: 0px;left: 0px;height:40px;width: 100%;text-align: right;"><strong>{{count($clients)}}</strong> Patients shown</div>
                 </div>

                  @include('client.modals.index')
            </div>
         </div>

@endsection
