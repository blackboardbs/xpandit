<!DOCTYPE html>
<html>
<head>
    <title>Patient Registration Email</title>
</head>

    <h2>New patient registration</h2>

    This email is to inform you that {{$name}} was registered as a new client on the {{env('APP_NAME')}} site.
    <br />
    <br />
    To approve this patient please click <a href="{{url('/clients/show/'.$link)}}">here</a>
    </body>

</html>